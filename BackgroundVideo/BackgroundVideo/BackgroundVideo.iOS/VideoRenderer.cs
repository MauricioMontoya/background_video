﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BackgroundVideo.iOS.Renderers;
using Foundation;
using MediaPlayer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using BackgroundVideo.Controls;
using System.IO;

[assembly: ExportRenderer(typeof(Video), typeof(VideoRenderer))]
namespace BackgroundVideo.iOS.Renderers
{
    class VideoRenderer : ViewRenderer<Video, UIView>
    {
        MPMoviePlayerController videoPlayer;
        NSObject notification = null;

        void InitVideoPlayer()
        {
            var path = Path.Combine(NSBundle.MainBundle.BundlePath, Element.Source);

            if (!NSFileManager.DefaultManager.FileExists(path))
            {
                Console.WriteLine("Video not exist");
                videoPlayer = new MPMoviePlayerController();
                videoPlayer.ControlStyle = MPMovieControlStyle.None;
                videoPlayer.ScalingMode = MPMovieScalingMode.AspectFill;
                videoPlayer.RepeatMode = MPMovieRepeatMode.One;
                videoPlayer.View.BackgroundColor = UIColor.Clear;
                SetNativeControl(videoPlayer.View);
                return;
            }

            // Load the video from the app bundle.  
            NSUrl videoURL = new NSUrl(path, false);

            // Create and configure the movie player.  
            videoPlayer = new MPMoviePlayerController(videoURL);

            videoPlayer.ControlStyle = MPMovieControlStyle.None;
            videoPlayer.ScalingMode = MPMovieScalingMode.AspectFill;
            videoPlayer.RepeatMode = Element.Loop ? MPMovieRepeatMode.One : MPMovieRepeatMode.None;
            videoPlayer.View.BackgroundColor = UIColor.Clear;
            foreach (UIView subView in videoPlayer.View.Subviews)
            {
                subView.BackgroundColor = UIColor.Clear;
            }

            videoPlayer.PrepareToPlay();
            SetNativeControl(videoPlayer.View);
        }
    }
}